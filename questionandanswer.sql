-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 20 2020 г., 19:06
-- Версия сервера: 10.3.22-MariaDB
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `questionandanswer`
--

-- --------------------------------------------------------

--
-- Структура таблицы `clientresult`
--

CREATE TABLE `clientresult` (
  `Name` varchar(100) NOT NULL,
  `Rating` int(100) NOT NULL,
  `Id` int(100) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `clientresult`
--

INSERT INTO `clientresult` (`Name`, `Rating`, `Id`) VALUES
('Аня', 1, 79),
('Вася', -1, 101),
('Вася пупкин', 0, 102),
('Введите имя!', 1, 103);

-- --------------------------------------------------------

--
-- Структура таблицы `theme1`
--

CREATE TABLE `theme1` (
  `Question` varchar(100) NOT NULL,
  `FirstAnswer` varchar(100) NOT NULL,
  `SecoundAnswer` varchar(100) NOT NULL,
  `ThridAnswer` varchar(100) NOT NULL,
  `CorrectAnswer` varchar(100) NOT NULL,
  `id` int(100) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `theme1`
--

INSERT INTO `theme1` (`Question`, `FirstAnswer`, `SecoundAnswer`, `ThridAnswer`, `CorrectAnswer`, `id`) VALUES
('Где раки зимуют?', 'В подводных норах пресных водоёмов', 'На горе', 'На море', 'В подводных норах пресных водоёмов', 1),
('Какие жуки носят название того месяца, в котором появляются?', 'Божья коровка', 'Майский жук', 'Январский богомол', 'Майский жук', 2),
('Кого называют лесной «фонарик»?', 'Божья коровка', 'Муха', 'Светлячок', 'Светлячок', 3),
('Сколько ног у пауков?', 'Две', 'Восемь', 'Четыре', 'Восемь', 4),
('Какой моллюск быстрее всех плавает?', 'Осьминог ', 'Каракатица', 'Кальмар', 'Кальмар', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `theme2`
--

CREATE TABLE `theme2` (
  `Question` varchar(100) NOT NULL,
  `FirstAnswer` varchar(100) NOT NULL,
  `SecoundAnswer` varchar(100) NOT NULL,
  `ThridAnswer` varchar(100) NOT NULL,
  `CorrectAnswer` varchar(100) NOT NULL,
  `id` int(100) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `theme2`
--

INSERT INTO `theme2` (`Question`, `FirstAnswer`, `SecoundAnswer`, `ThridAnswer`, `CorrectAnswer`, `id`) VALUES
('Какой город является столицей Германии?', 'Кёльн', 'Берлин', 'Мюнхен', 'Берлин', 1),
('Какой город является столицей Дании?', 'Лиссабон', 'Сургут', 'Копенгаген', 'Копенгаген', 2),
('Самый большой океан?', 'Тихий', 'Атлантический', 'Северный ледовитый', 'Тихий', 3),
('Самое глубокое озеро?', 'Виктория', 'Байкал', 'Титикака', 'Байкал', 4),
('Самая маленькая часть света? ', 'Австралия', 'Африка', 'Европа', 'Австралия', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `theme3`
--

CREATE TABLE `theme3` (
  `Question` varchar(100) NOT NULL,
  `FirstAnswer` varchar(100) NOT NULL,
  `SecoundAnswer` varchar(100) NOT NULL,
  `ThridAnswer` varchar(100) NOT NULL,
  `CorrectAnswer` varchar(100) NOT NULL,
  `id` int(100) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `theme3`
--

INSERT INTO `theme3` (`Question`, `FirstAnswer`, `SecoundAnswer`, `ThridAnswer`, `CorrectAnswer`, `id`) VALUES
('Какое событие произошло на Руси в 988 году?', 'Избрание президента Путина В.В.', 'Крещение Руси', 'Уби́йство Аскольда и Дира', 'Крещение Руси', 1),
('Князь, который крестил Русь', 'Петр 1', 'Олег', 'Владимир', 'Владимир', 2),
('Имя последнего русского царя', 'Николай 2', 'Александр 1', 'Елизавета Петровна ', 'Николай 2', 3),
('Братья, которые изобрели славянскую азбуку?', 'Пожарский и Минин', 'Братья Гримм', 'Кирилл и Мефодий', 'Кирилл и Мефодий', 4),
('Имя первого царя из династии Романовых?', 'Михаил', 'Екатерина', 'Николай', 'Михаил', 5);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `clientresult`
--
ALTER TABLE `clientresult`
  ADD UNIQUE KEY `Id` (`Id`);

--
-- Индексы таблицы `theme1`
--
ALTER TABLE `theme1`
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `theme2`
--
ALTER TABLE `theme2`
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `theme3`
--
ALTER TABLE `theme3`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `clientresult`
--
ALTER TABLE `clientresult`
  MODIFY `Id` int(100) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT для таблицы `theme1`
--
ALTER TABLE `theme1`
  MODIFY `id` int(100) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `theme2`
--
ALTER TABLE `theme2`
  MODIFY `id` int(100) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `theme3`
--
ALTER TABLE `theme3`
  MODIFY `id` int(100) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
