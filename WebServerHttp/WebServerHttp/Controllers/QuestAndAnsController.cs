﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebServerHttp.Models;

namespace WebServerHttp.Controllers
{
    public class QuestAndAnsController : ApiController
    {
        // Get api/QuestAndAns
        public QuestionAndAnswers Get()
        {
            DBcs dBcs = new DBcs();
            MySqlCommand command = new MySqlCommand();
            command = new MySqlCommand("SELECT* FROM `Theme1` WHERE `id` = @idUsers", dBcs.getConnection());

            
            Random random = new Random();
            int value = random.Next(1, 2);
            command.Parameters.Add("@idUsers", MySqlDbType.Int32).Value = value;

            dBcs.openConnection();
            MySqlDataReader reader = command.ExecuteReader();
            reader.Read();
            QuestionAndAnswers qustionAndAnswer = new QuestionAndAnswers
            {
                question = reader[0].ToString(),
                firstAnswer = reader[1].ToString(),
                secoundAnswer = reader[2].ToString(),
                thridAnswer = reader[3].ToString(),
                correctAnswer = reader[4].ToString(),
                id = Convert.ToInt32(reader[5])
            };

            dBcs.closeConnection();
            return qustionAndAnswer;
        }
        // Get api/QuestAndAns/theme
        public QuestionAndAnswers Get(string theme)
        {
            DBcs dBcs = new DBcs();
            MySqlCommand command = new MySqlCommand();

            if (theme == "{Theme1}")
                command = new MySqlCommand("SELECT* FROM `Theme1` WHERE `id` = @idUsers", dBcs.getConnection());

            if (theme == "{Theme2}")
                command = new MySqlCommand("SELECT* FROM `Theme2` WHERE `id` = @idUsers", dBcs.getConnection());

            if (theme == "{Theme3}")
                command = new MySqlCommand("SELECT* FROM `Theme3` WHERE `id` = @idUsers", dBcs.getConnection());


            Random random = new Random();
            int value = random.Next(1, 6);
            dBcs.openConnection();
            command.Parameters.Add("@idUsers", MySqlDbType.Int32).Value = value;

            MySqlDataReader reader = command.ExecuteReader();
            reader.Read();

            QuestionAndAnswers qustionAndAnswer = new QuestionAndAnswers
            {
                question = reader[0].ToString(),
                firstAnswer = reader[1].ToString(),
                secoundAnswer = reader[2].ToString(),
                thridAnswer = reader[3].ToString(),
                correctAnswer = reader[4].ToString(),
                id = Convert.ToInt32(reader[5])
            };

            dBcs.closeConnection();
            return qustionAndAnswer;
        }

        // Post api/QuestAndAns
        public void Post([FromBody]AnswerFromClient answerFromClient)
        {
            DBcs dBcs = new DBcs();
            MySqlCommand command = new MySqlCommand("SELECT * FROM `clientresult` WHERE Name = @clientName", dBcs.getConnection());
            command.Parameters.Add("@clientName", MySqlDbType.VarChar).Value = answerFromClient.name;

            DataTable table = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();
            
            adapter.SelectCommand = command;
            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                
                dBcs.openConnection();
                MySqlDataReader reader = command.ExecuteReader();
                reader.Read();

                string name = reader[0].ToString();
                int rating = Convert.ToInt32(reader[1]);

                command = new MySqlCommand("DELETE  FROM `clientresult` WHERE Name = @clientName", dBcs.getConnection());
                command.Parameters.Add("@clientName", MySqlDbType.VarChar).Value = answerFromClient.name;

                if (answerFromClient.result == true)
                    rating++;
                else
                    rating--;

                reader.Close();
                dBcs.openConnection();
                command.ExecuteNonQuery();
                dBcs.closeConnection();

                command = new MySqlCommand("INSERT INTO `clientresult`(`Name`, `Rating`) VALUES(@nameC, @ratingC)",
                    dBcs.getConnection());

                command.Parameters.Add("@nameC", MySqlDbType.VarChar).Value = answerFromClient.name;
                command.Parameters.Add("@ratingC", MySqlDbType.Int32).Value = rating;

                dBcs.openConnection();
                command.ExecuteNonQuery();
                dBcs.closeConnection();

            }
            else
            {
                int rating = 0;
                if (answerFromClient.result == true)
                    rating++;
                else
                    rating--;

                command = new MySqlCommand("INSERT INTO `clientresult`(`Name`, `Rating`) VALUES(@nameC, @ratingC);",
                    dBcs.getConnection());

                command.Parameters.Add("@nameC", MySqlDbType.VarChar).Value = answerFromClient.name;
                command.Parameters.Add("@ratingC", MySqlDbType.Int32).Value = rating;

                dBcs.openConnection();
                command.ExecuteNonQuery();
                dBcs.closeConnection();
            }  
            
        }       
    }
}
