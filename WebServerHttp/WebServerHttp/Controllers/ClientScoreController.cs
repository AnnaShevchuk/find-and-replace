﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebServerHttp.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace WebServerHttp.Controllers
{
    public class ClientScoreController : ApiController
    {
        public ObservableCollection<ClientScore> Get()
        {
            DBcs dBcs = new DBcs();
            MySqlCommand command = new MySqlCommand();
            command = new MySqlCommand("SELECT * FROM `clientresult`", dBcs.getConnection());
            ObservableCollection<ClientScore> clientScores = new ObservableCollection<ClientScore>();
            dBcs.openConnection();
            MySqlDataReader reader = command.ExecuteReader();
            if(reader.HasRows)
            {

                while (reader.Read()) 
                {

                    ClientScore client = new ClientScore();
                    client.name = reader[0].ToString();
                    client.score = Convert.ToInt32(reader[1]);
                    clientScores.Add(client);
                }
                reader.Close();
                dBcs.closeConnection();
                return clientScores;
            }

            dBcs.closeConnection();
            return null;
        }
    }
}
