﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServerHttp.Models
{
    public class AnswerFromClient
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool result { get; set; }
        public string theme { get; set; }
        public string question { get; set; }
    }
}