﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServerHttp.Models
{
    public class ClientScore
    {
        public string name { get; set; }
        public int score { get; set; }
    }
}