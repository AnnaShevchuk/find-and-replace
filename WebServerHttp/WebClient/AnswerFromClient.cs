﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClient
{
    class AnswerFromClient
    {
        public string name { get; set; }
        public bool result { get; set; }
        public int id { get; set; }
        public string question { get; set; }
    }
}

