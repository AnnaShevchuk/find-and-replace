﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClient
{
    class ClientScore : INotifyPropertyChanged
    {
        public string name { get; set; }
        public int score { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
