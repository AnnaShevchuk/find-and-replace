﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClient
{
    public class QuestionAndAnswers
    {
        public string theme { get; set; }
        public string question { get; set; }
        public string firstAnswer { get; set; }
        public string secoundAnswer { get; set; }
        public string thridAnswer { get; set; }
        public string correctAnswer { get; set; }
        public int id{ get; set; }
    }
}
