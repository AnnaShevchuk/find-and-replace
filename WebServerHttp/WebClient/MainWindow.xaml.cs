﻿using EasyHttp.Http;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace WebClient
{
    public partial class MainWindow : Window
    {
        QuestionAndAnswers questAndAnswers;
        HttpClient client = new HttpClient();
        Question question = new Question();
        ObservableCollection<ClientScore> clientScores { get; set; } = new ObservableCollection<ClientScore>();

        public MainWindow()
        {

            InitializeComponent();
            this.DataContext = this;
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {

            for (int i = 0; i < question.Theme1.Length; i++)
            {
                question.Theme1[i] = false;
            }
            for (int i = 0; i < question.Theme2.Length; i++)
            {
                question.Theme2[i] = false;
            }
            for (int i = 0; i < question.Theme3.Length; i++)
            {
                question.Theme3[i] = false;
            }
        }

        private void FirstAnswerBlock_Click(object sender, RoutedEventArgs e)
        {
            AnswerFromClient answerFromClient = new AnswerFromClient
            {
                name = ClientName.Text,
                question = questAndAnswers.question,
            };

            if (Convert.ToString(FirstAnswerBlock.Content) == questAndAnswers.correctAnswer)
            {
                MessageBox.Show("Ответ правильный!");
                answerFromClient.result = true;
            }
            else
            {
                MessageBox.Show("Ответ нправильный!");
                answerFromClient.result = false;
            }

            var response = client.Post("https://localhost:44363/api/QuestAndAns", answerFromClient, HttpContentTypes.ApplicationJson);

            QuestBlock.Text = "";
            FirstAnswerBlock.Content = "";
            SecoundAnswerBlock.Content = "";
            ThridAnswerBlock.Content = "";

            ConnectButton.IsEnabled = true;
            Theme1.IsEnabled = true;
            Theme2.IsEnabled = true;
            Theme3.IsEnabled = true;
            FirstAnswerBlock.IsEnabled = false;
            SecoundAnswerBlock.IsEnabled = false;
            ThridAnswerBlock.IsEnabled = false;
        }


        private void SecoundAnswerBlock_Click(object sender, RoutedEventArgs e)
        {
            AnswerFromClient answerFromClient = new AnswerFromClient
            {
                name = ClientName.Text,
                question = questAndAnswers.question,
            };

            if (Convert.ToString(SecoundAnswerBlock.Content) == questAndAnswers.correctAnswer)
            {
                MessageBox.Show("Ответ правильный!");
                answerFromClient.result = true;
            }
            else
            {
                MessageBox.Show("Ответ неправильный!");
                answerFromClient.result = false;
            }

            var response = client.Post("https://localhost:44363/api/QuestAndAns", answerFromClient, HttpContentTypes.ApplicationJson);

            QuestBlock.Text = "";
            FirstAnswerBlock.Content = "";
            SecoundAnswerBlock.Content = "";
            ThridAnswerBlock.Content = "";

            ConnectButton.IsEnabled = true;
            Theme1.IsEnabled = true;
            Theme2.IsEnabled = true;
            Theme3.IsEnabled = true;
            FirstAnswerBlock.IsEnabled   = false;
            SecoundAnswerBlock.IsEnabled = false;
            ThridAnswerBlock.IsEnabled   = false;
        }

        private void ThridAnswerBlock_Click(object sender, RoutedEventArgs e)
        {
            AnswerFromClient answerFromClient = new AnswerFromClient
            {
                name = ClientName.Text,
                question = questAndAnswers.question,
            };

            if (Convert.ToString(ThridAnswerBlock.Content) == questAndAnswers.correctAnswer)
            {
                MessageBox.Show("Ответ правильный!");
                answerFromClient.result = true;
            }
            else
            {
                MessageBox.Show("Ответ неправильный!");
                answerFromClient.result = false;
            }

            var response = client.Post("https://localhost:44363/api/QuestAndAns", answerFromClient, HttpContentTypes.ApplicationJson);

            QuestBlock.Text = "";
            FirstAnswerBlock.Content = "";
            SecoundAnswerBlock.Content = "";
            ThridAnswerBlock.Content = "";

            ConnectButton.IsEnabled = true;
            Theme1.IsEnabled = true;
            Theme2.IsEnabled = true;
            Theme3.IsEnabled = true;
            FirstAnswerBlock.IsEnabled = false;
            SecoundAnswerBlock.IsEnabled = false;
            ThridAnswerBlock.IsEnabled = false;
        }

        private void Theme1_Click(object sender, RoutedEventArgs e)
        {
            bool checkAll = true;
            for (int i = 0; i < question.Theme1.Length; i++)
            {
                if (question.Theme1[i] == false)
                    checkAll = false;
            }

            if (checkAll == true)
            {
                MessageBox.Show("Вы ответили на все вопросы в этой теме!");
                return;
            }

            while (true)
            {
                var response = client.Get("https://localhost:44363/api/QuestAndAns?theme={Theme1}");
                questAndAnswers = response.StaticBody<QuestionAndAnswers>();
                if (question.Theme1[questAndAnswers.id - 1] == false)
                {
                    question.Theme1[questAndAnswers.id - 1] = true;
                    break;
                }
            }

            QuestBlock.Text = questAndAnswers.question;
            FirstAnswerBlock.Content = questAndAnswers.firstAnswer;
            SecoundAnswerBlock.Content = questAndAnswers.secoundAnswer;
            ThridAnswerBlock.Content = questAndAnswers.thridAnswer;

            ConnectButton.IsEnabled = false;
            Theme1.IsEnabled = false;
            Theme2.IsEnabled = false;
            Theme3.IsEnabled = false;
            FirstAnswerBlock.IsEnabled   = true;
            SecoundAnswerBlock.IsEnabled = true;
            ThridAnswerBlock.IsEnabled   = true;
        }

        private void Theme2_Click(object sender, RoutedEventArgs e)
        {
            bool checkAll = true;
            for (int i = 0; i < question.Theme2.Length; i++)
            {
                if (question.Theme2[i] == false)
                    checkAll = false;
            }

            if (checkAll == true)
            {
                MessageBox.Show("Вы ответили на все вопросы в этой теме!");
                return;
            }

            while (true)
            {
                var response = client.Get("https://localhost:44363/api/QuestAndAns?theme={Theme2}");
                questAndAnswers = response.StaticBody<QuestionAndAnswers>();
                if (question.Theme2[questAndAnswers.id - 1] == false)
                {
                    question.Theme2[questAndAnswers.id - 1] = true;
                    break;
                }
            }

            QuestBlock.Text = questAndAnswers.question;
            FirstAnswerBlock.Content = questAndAnswers.firstAnswer;
            SecoundAnswerBlock.Content = questAndAnswers.secoundAnswer;
            ThridAnswerBlock.Content = questAndAnswers.thridAnswer;

            ConnectButton.IsEnabled = false;
            Theme1.IsEnabled = false;
            Theme2.IsEnabled = false;
            Theme3.IsEnabled = false;
            FirstAnswerBlock.IsEnabled = true;
            SecoundAnswerBlock.IsEnabled = true;
            ThridAnswerBlock.IsEnabled = true;
        }

        private void Theme3_Click(object sender, RoutedEventArgs e)
        {
            bool checkAll = true;
            for (int i = 0; i < question.Theme3.Length; i++)
            {
                if (question.Theme3[i] == false)
                    checkAll = false;
            }

            if (checkAll == true)
            {
                MessageBox.Show("Вы ответили на все вопросы в этой теме!");
                return;
            }

            while (true)
            {
                var response = client.Get("https://localhost:44363/api/QuestAndAns?theme={Theme3}");
                questAndAnswers = response.StaticBody<QuestionAndAnswers>();
                if (question.Theme3[questAndAnswers.id - 1] == false)
                {
                    question.Theme3[questAndAnswers.id - 1] = true;
                    break;
                }
            }

            QuestBlock.Text = questAndAnswers.question;
            FirstAnswerBlock.Content = questAndAnswers.firstAnswer;
            SecoundAnswerBlock.Content = questAndAnswers.secoundAnswer;
            ThridAnswerBlock.Content = questAndAnswers.thridAnswer;

            ConnectButton.IsEnabled = false;
            Theme1.IsEnabled = false;
            Theme2.IsEnabled = false;
            Theme3.IsEnabled = false;
            FirstAnswerBlock.IsEnabled = true;
            SecoundAnswerBlock.IsEnabled = true;
            ThridAnswerBlock.IsEnabled = true;
        }

        private void ShowClientsScore_Click(object sender, RoutedEventArgs e)
        {

            var response = client.Get("https://localhost:44363/api/ClientScore");
            clientScores = response.StaticBody<ObservableCollection<ClientScore>>();
            if (clientScores == null)
                MessageBox.Show("Игроков еще не было");

            ListBoxClientsScore.ItemsSource = clientScores;

        }
    }
}




